# [queue-interop](https://phppackages.org/p/queue-interop/queue-interop)

[**queue-interop/queue-interop**](https://packagist.org/packages/queue-interop/queue-interop)
[![rank](https://pages-proxy.gitlab.io/phppackages.org/queue-interop/queue-interop/rank.svg)](http://phppackages.org/p/queue-interop/queue-interop)
Promoting the interoperability of message queue objects. Based on Java JMS

## Usage examples
* [Spool Swiftmailer emails to real message queue](https://blog.forma-pro.com/spool-swiftmailer-emails-to-real-message-queue-9ecb8b53b5de)
